#!/usr/bin/env python

# Edit this script to add your team's training code.
# Some functions are *required*, but you can edit most parts of the required functions, remove non-required functions, and add your own functions.

################################################################################
#
# Imported functions and variables
#
################################################################################

# Import functions. These functions are not required. You can change or remove them.
from helper_code import *
import numpy as np, os, sys, joblib
import pandas as pd
import h5py
import math
from scipy import signal
from ecgdetectors import Detectors
detectors = Detectors(250)
import torch
import torchvision
from torchvision import transforms
from torch.utils.data import TensorDataset, DataLoader, Dataset
import torch.nn as nn
from functools import partial
import tsfel
import hrvanalysis
from sklearn.model_selection import KFold
from sklearn.metrics import accuracy_score

# Define the Challenge lead sets. These variables are not required. You can change or remove them.
twelve_leads = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
six_leads = [0, 1, 2, 3, 4, 5]
four_leads = [0, 1, 2, 7]
three_leads = [0, 1, 7]
two_leads = [0, 1]
lead_sets = [twelve_leads, six_leads, four_leads, three_leads, two_leads]

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

################################################################################
#
# Pre-processing Functions
#
################################################################################
# Low pass anti-aliasing butterworth function
# Author: Jamal Esmaelpoor
def ECG_lowpass_anti_aliasing(record,sfreq,nsample ):
    
    cutoff_freq = 125
    order = 16
    normalized_cutoff_freq = 2 * cutoff_freq / sfreq

    numerator_coeffs, denominator_coeffs = signal.butter(order, normalized_cutoff_freq)

    time = np.linspace(0, nsample/sfreq, nsample, endpoint=False)
    
    filtered_record = np.zeros(record.shape)
    for i in range(record.shape[0]):
        sig = record[i,:]
        filtered_record[i,:] = signal.lfilter(numerator_coeffs, denominator_coeffs, sig)

    return filtered_record

# High pass butterworth function
# Author: Jamal Esmaelpoor
def ECG_highpass(record,sfreq,nsample ):
    
    order = 3
    lowcut = 0.1
    #highcut = 50
    nyq = 0.5 * sfreq
    low = lowcut / nyq
    #high = highcut / nyq
    numerator_coeffs, denominator_coeffs = signal.butter(order, low, btype='high')
    
    filtered_record = np.zeros(record.shape)
    for i in range(record.shape[0]):
        sig = record[i,:]
        filtered_record[i,:] = signal.lfilter(numerator_coeffs, denominator_coeffs, sig)

    return filtered_record

# Low pass butterworth function
# Author: Jamal Esmaelpoor
def ECG_lowpass(record,sfreq,nsample ):
    
    cutoff_freq = 30
    order = 10
    normalized_cutoff_freq = 2 * cutoff_freq / sfreq

    numerator_coeffs, denominator_coeffs = signal.butter(order, normalized_cutoff_freq)

    time = np.linspace(0, nsample/sfreq, nsample, endpoint=False)
    
    filtered_record = np.zeros(record.shape)
    for i in range(record.shape[0]):
        sig = record[i,:]
        filtered_record[i,:] = signal.lfilter(numerator_coeffs, denominator_coeffs, sig)

    return filtered_record

# Find the LCD for resampling our data to 250 Hz
# Author: Jamal Esmaelpoor
def lcm(x, y):  
    if x > y:   
        greater = x  
    else:  
        greater = y  
        
    while(True):  
        if((greater % x == 0) and (greater % y == 0)):  
            lcm = greater  
            break  
        greater += 1  
        
    return int(lcm)

# Extract features using a variety of feature extraction libraries, such as hrv-analysis and TSFEL
# Author: Lachlan Burne
def featureExtraction(rr_intervals, ECG):
    time_domain = hrvanalysis.get_time_domain_features(rr_intervals)
    frequency_domain = hrvanalysis.get_frequency_domain_features(rr_intervals)
    non_linear_domain = hrvanalysis.get_csi_cvi_features(rr_intervals)

    HRV = [time_domain['mean_nni'], time_domain['cvnni']]
    Time_Features = [time_domain['cvsd'], time_domain['max_hr'], time_domain['mean_hr'], time_domain['median_nni'], time_domain['min_hr'], time_domain['nni_20'], time_domain['nni_50'], time_domain['pnni_20'], 
                                                    time_domain['pnni_50'], time_domain['range_nni'], time_domain['rmssd'], time_domain['sdnn'], time_domain['sdsd'], time_domain['std_hr']]

    Frequency_Features = [frequency_domain['lf'], frequency_domain['hf'], frequency_domain['lf_hf_ratio'], frequency_domain['lfnu'], frequency_domain['hfnu'], frequency_domain['total_power'], frequency_domain['vlf']]
    Non_Linear_Features = [non_linear_domain['csi'], non_linear_domain['cvi']]

    # Retrieves a pre-defined feature configuration file to extract all available features
    cfg = tsfel.get_features_by_domain()

    # Extract features
    ECG = pd.DataFrame(ECG)
    X = tsfel.time_series_features_extractor(cfg, ECG, fs=250)
    return np.float64(np.concatenate((np.array(HRV), np.array(Time_Features), np.array(Frequency_Features), np.array(Non_Linear_Features), np.array(X).flatten())))

################################################################################
#
# Model Creation Function
#
################################################################################
# Custom convolution method that automatically pads the inputs to maintain dimensionality
class Conv2dAuto(nn.Conv2d):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.padding = (self.kernel_size[0]//2, self.kernel_size[1]//2)

        
conv3x3 = partial(Conv2dAuto, kernel_size=3, bias=False) # Used in model creation

# Activation function library makes it easy to quickly test various activation functions
def activation_function(activation):
    return nn.ModuleDict([['relu', nn.ReLU(inplace=True)],
                        ['leaky_relu', nn.LeakyReLU(negative_slope=0.01, inplace=True)],
                        ['selu', nn.SELU(inplace=True)],
                        ['silu', nn.SiLU(inplace=True)],
                        ['prelu', nn.PReLU()],
                        ['none', nn.Identity()]
                        ])[activation]

# Fundamental residual block for the ResNet architecture
class ResBlock(nn.Module):
    def __init__(self, in_channels, out_channels, activation='relu'):
        super().__init__()
        self.in_channels, self.out_channels, self.activation = in_channels, out_channels, activation
        self.blocks = nn.Identity()
        self.activate = activation_function(activation)
        self.shortcut = nn.Identity()

    def forward(self, x):
        residual = x
        # Adjusts channel width of residual connection when input and output channels do not match
        if self.should_apply_shortcut: residual = self.shortcut(x) 
        x = self.blocks(x)
        x += residual
        x = self.activate(x)
        return x

    @property
    def should_apply_shortcut(self):
        return self.in_channels != self.out_channels

# Block used to adjust the channel width of a residual connection.
class ResNetResBlock(ResBlock):
    def __init__(self, in_channels, out_channels, expansion=1, downsampling=1, conv=conv3x3, *args, **kwargs):
        super().__init__(in_channels, out_channels, *args, **kwargs)
        self.expansion, self.downsampling, self.conv = expansion, downsampling, conv
        self.shortcut = nn.Sequential(
            nn.Conv2d(self.in_channels, self.expanded_channels, kernel_size=1, stride=self.downsampling, bias=False),
            nn.BatchNorm2d(self.expanded_channels)
        ) if self.should_apply_shortcut else None

    @property
    def expanded_channels(self):
        return self.out_channels * self.expansion

    @property
    def should_apply_shortcut(self):
        return self.in_channels != self.expanded_channels

# Method to automatically apply a batch norm after convolution
def conv_batch(in_channels, out_channels, conv, *args, **kwargs):
    return nn.Sequential(conv(in_channels, out_channels, *args, **kwargs), nn.BatchNorm2d(out_channels))

# Squeeze and Excitation block used to adjust the weighting of each channel
class SE_Block(nn.Module):
    def __init__(self, c, r=16):
        super().__init__()
        self.squeeze = nn.AdaptiveAvgPool2d(1)
        self.excitation = nn.Sequential(
            nn.Linear(c, c // r, bias=False),
            activation_function('relu'),
            nn.Linear(c // r, c, bias=False),
            nn.Sigmoid()
        )
    
    def forward(self, x):
        bs, c, _, _ = x.shape
        y = self.squeeze(x).view(bs, c)
        y = self.excitation(y).view(bs, c, 1, 1)
        return x * y.expand_as(x)

# Basic Resnet block for ResNet18 and ResNet34
class ResBasic(ResNetResBlock):
  # Two layers of 3x3 convolution, bn, activation
    expansion = 1
    def __init__(self, in_channels, out_channels, *args, **kwargs):
        super().__init__(in_channels, out_channels, *args, **kwargs)
        self.blocks = nn.Sequential(
            conv_batch(self.in_channels, self.out_channels, conv=self.conv, bias=False, stride=self.downsampling),
            activation_function(self.activation),
            conv_batch(self.out_channels, self.expanded_channels, conv=self.conv, bias=False),
        )

# Modified Basic block to include the Squeeze and Excitation block
class ResSE(ResNetResBlock):
  # Two layers of 3x3 convolution, bn, activation
    expansion = 1
    def __init__(self, in_channels, out_channels, *args, **kwargs):
        super().__init__(in_channels, out_channels, *args, **kwargs)
        self.blocks = nn.Sequential(
            conv_batch(self.in_channels, self.out_channels, conv=self.conv, bias=False, stride=self.downsampling),
            activation_function(self.activation),
            nn.Dropout(p=0.2),
            conv_batch(self.out_channels, self.expanded_channels, conv=self.conv, bias=False),
            SE_Block(self.expanded_channels)
        )
    
# Bottleneck block for ResNet50 and above
class ResBottle(ResNetResBlock):
    expansion = 4
    def __init__(self, in_channels, out_channels, *args, **kwargs):
        super().__init__(in_channels, out_channels, expansion=4, *args, **kwargs)
        self.blocks = nn.Sequential(
            conv_batch(self.in_channels, self.out_channels, self.conv, kernel_size=1),
            activation_function(self.activation),
            conv_batch(self.out_channels, self.out_channels, self.conv, kernel_size=3, stride=self.downsampling),
            activation_function(self.activation),
            conv_batch(self.out_channels, self.expanded_channels, self.conv, kernel_size=1)
        )

# A single layer in a ResNet model
class ResLayer(nn.Module):
  # ResNet Layer composed of n blocks
  def __init__(self, in_channels, out_channels, block=ResBasic, n=1, *args, **kwargs):
    super().__init__()
    downsampling = 2 if in_channels != out_channels else 1
    self.blocks = nn.Sequential(
        block(in_channels, out_channels, *args, **kwargs, downsampling=downsampling),
        *[block(out_channels * block.expansion, out_channels, downsampling=1, *args, **kwargs) for _ in range(n-1)]
    )
  
  def forward(self, x):
    x = self.blocks(x)
    return x

# Encoder block of a ResNet model. Includes the gate, plus a sequence of ResNet layers
class ResEncoder(nn.Module):
    def __init__(self, in_channels=1, blocks_sizes=[64, 128, 256, 512], depths=[2,2,2,2], activation='relu', block=ResBasic, leads=[0,1,2,3,4,5,6,7,8,9,10,11], *args, **kwargs):
        super().__init__()
        self.blocks_sizes = blocks_sizes
        self.conv_kernel_size = 7 if len(leads) >= 6 else (2, 7)
        self.pool_kernel_size = 3 if len(leads) >= 6 else (2, 3)
        self.gate = nn.Sequential(
            nn.Conv2d(in_channels, self.blocks_sizes[0], kernel_size=self.conv_kernel_size, stride=(1, 2), padding=3, bias=False),
            nn.BatchNorm2d(self.blocks_sizes[0]),
            activation_function(activation),
            nn.MaxPool2d(kernel_size=self.pool_kernel_size, stride=(1,2), padding=1)
        )

        self.in_out_block_sizes = list(zip(blocks_sizes, blocks_sizes[1:]))
        self.blocks = nn.ModuleList([
                                     ResLayer(blocks_sizes[0], blocks_sizes[0], n=depths[0], activation=activation, block=block, *args, **kwargs),
                                     *[ResLayer(in_channels * block.expansion, out_channels, n=n, activation=activation, block=block, *args, **kwargs)
                                     for (in_channels, out_channels), n in zip(self.in_out_block_sizes, depths[1:])]
        ])
    
    def forward(self, x):
        x = self.gate(x)
        for block in self.blocks:
            x = block(x)
        return x

# Converts the output of the encoder into a 1D array to input into MLP
class ResDecoder(nn.Module):
    def __init__(self, in_features, n_classes):
        super().__init__()
        self.avg = nn.AdaptiveAvgPool2d((1,1))
        self.decoder = nn.Linear(in_features, n_classes)
        
    def forward(self, x, x2):
        x = self.avg(x)
        x = x.view(x.size(0), -1)
        x2 = x2.view(x2.size(0), -1)
        x = torch.cat((x,x2), dim=1)
        x = self.decoder(x)
        return x

# Entire ResNet model
class ResNet(nn.Module):
    def __init__(self, in_channels, n_classes, m_features, leads, *args, **kwargs):
        super().__init__()
        self.encoder = ResEncoder(in_channels, leads=leads, activation='relu', *args, **kwargs)
        self.decoder = ResDecoder(self.encoder.blocks[-1].blocks[-1].expanded_channels + n_classes, n_classes)
        self.feature_extract1 = nn.Linear(m_features, 128)
        self.feature_extract2 = nn.Linear(128, 64)
        self.feature_extract3 = nn.Linear(64, 32)
        self.feature_extract4 = nn.Linear(32, n_classes)
        self.activate = activation_function('relu')
        self.dropout = nn.Dropout(p=0.2)
        
    def forward(self, x, x2):
        x2 = self.feature_extract1(x2)
        x2 = self.activate(x2)
        x2 = self.dropout(x2)
        x2 = self.feature_extract2(x2)
        x2 = self.activate(x2)
        x2 = self.dropout(x2)
        x2 = self.feature_extract3(x2)
        x2 = self.activate(x2)
        x2 = self.dropout(x2)
        x2 = self.feature_extract4(x2)
        x2 = self.activate(x2)
        x2 = self.dropout(x2)
        x = self.encoder(x)
        x = self.decoder(x, x2)
        return x

# Create a ResNet18
def resnet18(in_channels, n_classes, m_features, block=ResBasic, leads=[0,1,2,3,4,5,6,7,8,9,10,11], *args, **kwargs):
    return ResNet(in_channels, n_classes, m_features, block=block, leads=leads, depths=[2,2,2,2], *args, **kwargs)

# Create a ResNet34
def resnet34(in_channels, n_classes, m_features, block=ResBasic, leads=[0,1,2,3,4,5,6,7,8,9,10,11], *args, **kwargs):
    return ResNet(in_channels, n_classes, m_features, block=block, leads=leads, depths=[3, 4, 6, 3], *args, **kwargs)

# Create a SE-ResNet34
def se_resnet34(in_channels, n_classes, m_features, block=ResSE, leads=[0,1,2,3,4,5,6,7,8,9,10,11], *args, **kwargs):
    return ResNet(in_channels, n_classes, m_features, block=block, leads=leads, depths=[3, 4, 6, 3], *args, **kwargs)

# Create ResNet50
def resnet50(in_channels, n_classes, m_features, block=ResBottle, leads=[0,1,2,3,4,5,6,7,8,9,10,11], *args, **kwargs):
    return ResNet(in_channels, n_classes, m_features, block=block, leads=leads, depths=[3, 4, 6, 3], *args, **kwargs)

################################################################################
#
# Training model function
#
################################################################################

# Train your model. 
# Author: Lachlan Burne, Physionet Challenge
def training_code(data_directory, model_directory):
    #'''
    # Find header and recording files.
    print('Finding header and recording files...')

    header_files, recording_files = find_challenge_files(data_directory)
    num_recordings = len(recording_files)

    if not num_recordings:
        raise Exception('No data was provided.')

    # Create a folder for the model if it does not already exist.
    if not os.path.isdir(model_directory):
        os.mkdir(model_directory)

    # Extract the classes from the dataset.
    print('Extracting classes...')
    scored_arr = {'164889003':'AF', '164890007': 'AFL', '6374002': 'BBB', '426627000': 'Brady', '733534002': 'CLBBB', 
    '713427006': 'CRBBB', '270492004': 'IAVB', '713426002': 'IRBBB', '39732003': 'LAD', '445118002': 'LAnFB', '164947007': 'LPR',
    '251146004': 'LQRSV', '111975006': 'LQT', '698252002': 'NSIVCB', '426783006': 'NSR', '284470004': 'PAC', '10370003': 'PR', 
    '365413008': 'PRWP', '427172004': 'PVC', '164917005': 'QAb', '47665007': 'RAD', '427393009': 'SA', '426177001': 'SB', 
    '427084000': 'STach', '164934002': 'TAb', '59931005': 'TInv', '164909002': 'LBBB', '59118001': 'RBBB', '63593006': 'SVPB', 
    '17338001': 'VPB'}
    scored_df  = pd.DataFrame(data = np.zeros(60).reshape(30,2),columns=(['Arrhythmia Numerical Code','Arrhythmia Abbrivation']))
    scored_df['Arrhythmia Numerical Code'] = scored_arr.keys()
    scored_df['Arrhythmia Abbrivation'] = scored_arr.values()
    scored_classes = list(scored_arr.keys())
    num_scored_classes = len(scored_classes)

    
    classes = set()
    for header_file in header_files:
        header = load_header(header_file)
        classes |= set(get_labels(header))
    if all(is_integer(x) for x in classes):
        classes = sorted(classes, key=lambda x: int(x)) # Sort classes numerically if numbers.
    else:
        classes = sorted(classes) # Sort classes alphanumerically if not numbers.
    num_classes = len(classes)
    
    # Calculate class weighting
    class_count = np.zeros(26)
    
    # Extract the features and labels from the dataset.
    print('Extracting features and labels...')
    
    data = np.zeros((1, 14), dtype=np.float32) # 14 features: one feature for each lead, one feature for age, and one feature for sex
    f = h5py.File('Dataset.h5', 'a')
    
    for i in range(num_recordings):
        if (i+1) % 250 == 0:
            print('    {}/{}...'.format(i+1, num_recordings))

        # Load header and recording.
        header = load_header(header_files[i])
        recording = load_recording(recording_files[i])

        # Get age, sex and root mean square of the leads.
        age, sex, rms = get_features(header, recording, twelve_leads)
        if not is_integer(age):
            age = 50
        if not is_integer(sex):
            sex = 0.5
        data[0, 0:12] = rms
        data[0, 12] = age
        data[0, 13] = sex
        
        # Assign target labels
        rlabels = np.zeros((1,num_scored_classes))
        current_labels = get_labels(header)
        for label in current_labels:
            if label in scored_classes:
                j = scored_classes.index(label)
                rlabels[0, j] = 1
        if sum(rlabels.transpose()) == 0:
            continue

        # Amend target labels
        rlabels26 = np.zeros((1,26))
        rlabels26 = rlabels[0,0:26].reshape(1,26)
        if rlabels[0,26]==1:
            rlabels26[0,4]=1
        if rlabels[0,27]==1:
            rlabels26[0,5]=1
        if rlabels[0,28]==1:
            rlabels26[0,15]=1
        if rlabels[0,29]==1:
            rlabels26[0,18]=1
        
        # Chek if all leads are available
        all_leads = 1
        for j in range(12):
            if recording[j,:].mean()==0 and recording[j,:].var()==0:
                all_leads = 0
        if all_leads==0:
            continue
        
        # Dynamically calculate the weights of each class in the training datase
        class_count = np.add(class_count, rlabels26.flatten())
        
        recording_filtered = filter_ecg(recording, header)
        
        # If sampling is at 250Hz, then a 10s fixed window is 2500 samples. Any window smaller than 2500 will be zero padded.
        if recording_filtered.shape[1] >= 2500:
            temp = recording_filtered[:,:2500]
            if np.isnan(temp).all():
                temp = np.ones((12,2500)) # in case of complete corruption
            elif np.isnan(temp).any():
                temp = np.apply_along_axis(interp, 1, temp) # interpolate nan values
            #if not np.isnan(temp).any():
            temp1 = temp[0].flatten()
            r_peaks = np.array(detectors.pan_tompkins_detector(temp[0])) #Take HRV features from one lead
            rr_interval = np.diff(r_peaks)
            if not np.any(rr_interval) or len(rr_interval) < 2:
                rr_interval = np.ones(10)*250 # Take average heartbeat of 1 second times the sampling rate. In case no r_peaks detected.
            
            # Block Print
            blockPrint()
            features = featureExtraction(rr_interval, temp1)
            # Enable Print
            enablePrint()
            
            features = np.concatenate((features[None,:], data), axis=1)
            if np.isnan(features).any():
                features = interp(features)
            temp = temp[None, None, :, :]
            features = features[None, :, :]
            labels = rlabels26[None, : ,:]
            if i == 0:
                # Create the dataset at first
                f.create_dataset('data', data=temp, compression="gzip", chunks=True, maxshape=(None, None, temp.shape[2],temp.shape[3]))
                f.create_dataset('feat', data=features, compression="gzip", chunks=True, maxshape=(None, features.shape[1], features.shape[2]))
                f.create_dataset('label', data=labels, compression="gzip", chunks=True, maxshape=(None, labels.shape[1], labels.shape[2])) 
            else:
                # Append new data to it
                f['data'].resize((f['data'].shape[0] + temp.shape[0]), axis=0)
                f['data'][-temp.shape[0]:] = temp

                f['feat'].resize((f['feat'].shape[0] + features.shape[0]), axis=0)
                f['feat'][-features.shape[0]:] = features

                f['label'].resize((f['label'].shape[0] + labels.shape[0]), axis=0)
                f['label'][-labels.shape[0]:] = labels

        else:
            temp = np.zeros((12,2500))
            temp[:recording_filtered.shape[0], :recording_filtered.shape[1]] = recording_filtered
            if np.isnan(temp).all():
                temp = np.ones((12,2500)) # in case of complete corruption
            elif np.isnan(temp).any():
                temp = np.apply_along_axis(interp, 1, temp) # interpolate nan values
            temp1 = temp[0].flatten()
            r_peaks = np.array(detectors.pan_tompkins_detector(temp[0])) # Take HRV features from one lead
            rr_interval = np.diff(r_peaks)
            if not np.any(rr_interval):
                rr_interval = np.ones(10)*250 # Take average heartbeat of 1 second times the sampling rate. In case no r_peaks detected.
            
            # Block Print
            blockPrint()
            features = featureExtraction(rr_interval, temp1)
            # Enable Print
            enablePrint()
            
            features = np.concatenate((features[None, :], data), axis=1)
            if np.isnan(features).any():
                features = interp(features)
            temp = temp[None, None, :, :]
            features = features[None, :, :]
            labels = rlabels26[None, : ,:]
            if i == 0:
                # Create the dataset at first
                f.create_dataset('data', data=temp, compression="gzip", chunks=True, maxshape=(None, None, temp.shape[2],temp.shape[3]))
                f.create_dataset('feat', data=features, compression="gzip", chunks=True, maxshape=(None, features.shape[1], features.shape[2]))
                f.create_dataset('label', data=labels, compression="gzip", chunks=True, maxshape=(None, labels.shape[1], labels.shape[2])) 
            else:
                # Append new data to it
                f['data'].resize((f['data'].shape[0] + temp.shape[0]), axis=0)
                f['data'][-temp.shape[0]:] = temp

                f['feat'].resize((f['feat'].shape[0] + features.shape[0]), axis=0)
                f['feat'][-features.shape[0]:] = features

                f['label'].resize((f['label'].shape[0] + labels.shape[0]), axis=0)
                f['label'][-labels.shape[0]:] = labels

    f.close()

    #Class weight calculations
    N = np.sum(class_count)
    gamma = 1
    class_weights = np.zeros(26)
    for i in range(len(class_weights)):
        class_weights[i] = math.log((N-class_count[i]+gamma)/(class_count[i]+1))
    
    with h5py.File('class_weights.h5', 'w') as hf:
        hf.create_dataset("class_weights", data=class_weights)
    
    #'''
    # Import calculated class weights
    with h5py.File('class_weights.h5', 'r') as hf:
        class_weights = hf['class_weights'][:]
    class_weights = torch.Tensor(class_weights).to(device)
    
    # Import dataset
    with h5py.File('Dataset.h5', 'r') as hf:
        feature_data = hf['feat'][:]
        labels = hf['label'][:]
        ecg_data = hf['data'][:]

    # Calculate maximum and minimum feature values for normalisation
    feature_max = -10000000*np.ones(feature_data[0].shape[1])
    feature_min = 10000000*np.ones(feature_data[0].shape[1])
    
    # Max-Min normalisation
    for i in range(len(feature_data)):
        for j in range(feature_max.shape[0]):
            if feature_data[i][0][j] < feature_min[j]:
                feature_min[j] = feature_data[i][0][j]
            elif feature_data[i][0][j] > feature_max[j]:
                feature_max[j] = feature_data[i][0][j]
    
    # Remove edge cases such as zero variance and corrupted values
    bad_index = (feature_max == feature_min)
    inf_index = np.isinf(feature_min)
    del_index = bad_index | inf_index
    feature_max = np.delete(feature_max, del_index)
    feature_min = np.delete(feature_min, del_index)
    temp = np.zeros((feature_data.shape[0], feature_data.shape[1], len(feature_max)))
    for i in range(feature_data.shape[0]):
        temp[i][0] = np.delete(feature_data[i][0], del_index)
    feature_data = temp
    
    # Save normalisation values
    with h5py.File('feature_data.h5', 'w') as hf:
        hf.create_dataset("feature_max",  data=feature_max)
        hf.create_dataset("feature_min", data=feature_min)
        hf.create_dataset("del_index", data=del_index)
    
    # Normalize the training data 
    for i in range(feature_data.shape[0]):     
        for j in range(feature_max.shape[0]):
            if (feature_max[j]-feature_min[j]) == 0:
                feature_data[i][0][j] = 1
            else:
                feature_data[i][0][j] = (feature_data[i][0][j]-feature_min[j])/(feature_max[j]-feature_min[j])
    
    # Save normlized dataset
    with h5py.File('Normalised_Features.h5', 'w') as hf:
        hf.create_dataset("feat", data=feature_data)
    
    # Train a model for each lead set
    lead_index = 0 # Use to index pyplot figures
    for leads in lead_sets:
        print('Training model for {}-lead set'.format(len(leads)))
        e_data = ecg_data.copy()
        e_data = e_data[:, :, leads, :]
    
        #Config options
        k_folds = 5
        max_epoch = 30
        sigma = 2
        loss_fn = nn.BCEWithLogitsLoss(weight=(class_weights+sigma))
        batch_size = 32
        lr = 3e-4
        
        #Results
        results = {}
        best_acc = np.zeros(k_folds)
        
        #Set Fixed Seed
        torch.manual_seed(42)

        #Dataset
        dataset = torch.utils.data.TensorDataset(torch.FloatTensor(e_data), torch.FloatTensor(feature_data), torch.FloatTensor(labels))
        
        #Define K-Fold Cross Validator
        kfold = KFold(n_splits=k_folds, shuffle=True)
        
        #K-Fold Cross-Validation
        for fold, (train_ids, test_ids) in enumerate(kfold.split(dataset)):
            # New model directory for each fold
            new_model_directory = model_directory + f'\\Fold{fold+1}'
            
            print(f'FOLD {fold}')
            print('--------------------------------')
            
            # Sample elements randomly from a given list of ids, no replacement.
            train_subsampler = torch.utils.data.SubsetRandomSampler(train_ids)
            test_subsampler = torch.utils.data.SubsetRandomSampler(test_ids)
            
            # Define data loaders for training and testing data in this fold
            trainloader = torch.utils.data.DataLoader(
                              dataset, 
                              batch_size=batch_size, sampler=train_subsampler)
            testloader = torch.utils.data.DataLoader(
                              dataset,
                              batch_size=batch_size, sampler=test_subsampler)

            #Initialize model
            model = se_resnet34(1, labels[0].shape[1], feature_data[0].shape[1], leads=leads).to(device)
            model.apply(reset_weights)

            #Initialize optimizer
            optimizer = torch.optim.Adam(model.parameters(), lr=lr)
            #Test Scheduler
            scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min', factor=0.1, patience=2)
            
            #Result configuration
            accuracy = np.zeros(max_epoch)
            train_accuracy = np.zeros(max_epoch)
            val_loss = np.zeros(max_epoch)
            train_loss = np.zeros(max_epoch)
            temp_val = 1000000

            final_thresholds = np.array([0.41, 0.92, 0.28, 0.75, 0.34, 0.8, 0.5, 0.48, 
                                             0.48, 0.89, 0.51, 0.16, 0.05, 0.91, 0.31, 0.53, 
                                             0.73, 0.4, 0.37, 0.47, 0.49, 0.5, 0.22, 0.49, 0.66, 0.87])
            challenge_thresholds = np.array([0.05, 0.05, 0.09, 0.05, 0.07, 0.11, 0.16, 0.05, 0.3, 0.1, 0.12, 0.07, 
                                    0.05, 0.07, 0.05, 0.45, 0.19, 0.05, 0.22, 0.05, 0.16, 0.06, 0.14, 0.05, 0.06, 0.06])

            for epoch in range(max_epoch):
                train_loss[epoch], train_accuracy[epoch] = train(model, device, trainloader, optimizer, loss_fn, final_thresholds)
                val_loss[epoch], accuracy[epoch] = validate(model, device, testloader, loss_fn, final_thresholds)
                print("Epoch: {} Train Loss: {} Training Accuracy: {} Val Loss: {} Val Accuracy: {}".format(epoch+1, train_loss[epoch], train_accuracy[epoch], val_loss[epoch], accuracy[epoch]))
                if accuracy[epoch] > best_acc[fold]:
                    best_acc[fold] = accuracy[epoch]
                    save_model(new_model_directory, leads, model)
                if (epoch+1)%5==0:
                    if val_loss[epoch] > temp_val:
                        break
                    temp_val = val_loss[epoch]
                scheduler.step(val_loss[epoch])
                
            # Print accuracy
            print('Accuracy for fold %d: %d %%' % (fold, best_acc[fold]))
            print('--------------------------------')
            results[fold] = best_acc[fold]
            
        # Print fold results
        print(f'K-FOLD CROSS VALIDATION RESULTS FOR {k_folds} FOLDS')
        print('--------------------------------')
        sum_ = 0.0
        for key, value in results.items():
            print(f'Fold {key}: {value} %')
            sum_ += value
        print(f'Average: {sum_/len(results.items())} %')
        
        lead_index += 2


################################################################################
#
# Running trained model function
#
################################################################################

# Run your trained model. This function is *required*. You should edit this function to add your code, but do *not* change the arguments of this function.
def run_model(model, header, recording, leads):
    scored_arr = {'164889003':'AF', '164890007': 'AFL', '6374002': 'BBB', '426627000': 'Brady', '733534002': 'CLBBB', 
    '713427006': 'CRBBB', '270492004': 'IAVB', '713426002': 'IRBBB', '39732003': 'LAD', '445118002': 'LAnFB', '164947007': 'LPR',
    '251146004': 'LQRSV', '111975006': 'LQT', '698252002': 'NSIVCB', '426783006': 'NSR', '284470004': 'PAC', '10370003': 'PR', 
    '365413008': 'PRWP', '427172004': 'PVC', '164917005': 'QAb', '47665007': 'RAD', '427393009': 'SA', '426177001': 'SB', 
    '427084000': 'STach', '164934002': 'TAb', '59931005': 'TInv', '164909002': 'LBBB', '59118001': 'RBBB', '63593006': 'SVPB', 
    '17338001': 'VPB'}
    scored_df  = pd.DataFrame(data = np.zeros(60).reshape(30,2),columns=(['Arrhythmia Numerical Code','Arrhythmia Abbrivation']))
    scored_df['Arrhythmia Numerical Code'] = scored_arr.keys()
    scored_df['Arrhythmia Abbrivation'] = scored_arr.values()
    scored_classes = list(scored_arr.keys())
    num_scored_classes = len(scored_classes)
    
    # Extract the features and labels from the dataset.
    print('Extracting features and labels...')
    
    data = np.zeros((1, 14), dtype=np.float32) # 14 features: one feature for each lead, one feature for age, and one feature for sex
    
    # Get age, sex and root mean square of the leads.
    age, sex, rms = get_features(header, recording, twelve_leads)
    if not is_integer(age):
        age = 50
    if not is_integer(sex):
        sex = 0.5
    data[0, 0:12] = rms
    data[0, 12] = age
    data[0, 13] = sex

    # Assign target labels
    rlabels = np.zeros((1,num_scored_classes))
    current_labels = get_labels(header)
    for label in current_labels:
        if label in scored_classes:
            j = scored_classes.index(label)
            rlabels[0, j] = 1

    # Amend target labels
    rlabels26 = np.zeros((1,26))
    rlabels26 = rlabels[0,0:26].reshape(1,26)
    if rlabels[0,26]==1:
        rlabels26[0,4]=1
    if rlabels[0,27]==1:
        rlabels26[0,5]=1
    if rlabels[0,28]==1:
        rlabels26[0,15]=1
    if rlabels[0,29]==1:
        rlabels26[0,18]=1
    
    recording_filtered = filter_ecg(recording, header)
    
    # If sampling is at 250Hz, then a 10s fixed window is 2500 samples. Any window smaller than 2500 will be zero padded.
    if recording_filtered.shape[1] >= 2500:
        temp = recording_filtered[:,:2500]
        if np.isnan(temp).all():
            temp = np.ones((12,2500)) # in case of complete corruption
        elif np.isnan(temp).any():
            #temp = np.apply_along_axis(interp, 1, temp) # interpolate nan values
            temp = np.ones((12,2500)) # in case of complete corruption
        #if not np.isnan(temp).any():
        temp1 = temp[0].flatten()
        r_peaks = np.array(detectors.pan_tompkins_detector(temp[0])) #Take HRV features from one lead
        rr_interval = np.diff(r_peaks)
        if not np.any(rr_interval) or len(rr_interval) < 2:
            rr_interval = np.ones(10)*250 # Take average heartbeat of 1 second times the sampling rate. In case no r_peaks detected.
        
        # Block Print
        blockPrint() 
        features = featureExtraction(rr_interval, temp1)
        # Enable Print
        enablePrint() 
        
        features = np.concatenate((features[None,:], data), axis=1)
        if np.isnan(features).any():
            features = interp(features)
        data = temp[None, None, :, :]
        features = features[None, :, :]
        labels = rlabels26[None, : ,:]
    
    else:
        temp = np.zeros((12,2500))
        temp[:recording_filtered.shape[0], :recording_filtered.shape[1]] = recording_filtered
        if np.isnan(temp).all():
            temp = np.ones((12,2500)) # in case of complete corruption
        elif np.isnan(temp).any():
            temp = np.apply_along_axis(interp, 1, temp) # interpolate nan values
        temp1 = temp[0].flatten()
        r_peaks = np.array(detectors.pan_tompkins_detector(temp[0])) # Take HRV features from one lead
        rr_interval = np.diff(r_peaks)
        if not np.any(rr_interval):
            rr_interval = np.ones(10)*250 # Take average heartbeat of 1 second times the sampling rate. In case no r_peaks detected.
        
        # Block Print
        blockPrint() 
        features = featureExtraction(rr_interval, temp1)
        # Enable Print
        enablePrint() 
        
        features = np.concatenate((features[None, :], data), axis=1)
        if np.isnan(features).any():
            features = interp(features)
        data = temp[None, None, :, :]
        features = features[None, :, :]
        labels = rlabels26[None, : ,:]
    
    with h5py.File('feature_data.h5', 'r') as hf:
        feature_max = hf['feature_max'][:]
        feature_min = hf['feature_min'][:]
        del_index = hf['del_index'][:]
    
    temp = np.zeros((features.shape[0], features.shape[1], len(feature_max)))
    for i in range(features.shape[0]):
        temp[i][0] = np.delete(features[i][0], del_index)
    features = temp
    
    for i in range(feature_max.shape[0]):
        if (feature_max[i]-feature_min[i]) == 0:
            features[0][0][i] = 1
        else:
            features[0][0][i] = (features[0][0][i]-feature_min[i])/(feature_max[i]-feature_min[i])
    
    sigmoid = nn.Sigmoid()
    final_thresholds = torch.Tensor([0.41, 0.92, 0.28, 0.75, 0.34, 0.8, 0.5, 0.48, 
                                     0.48, 0.89, 0.51, 0.16, 0.05, 0.91, 0.31, 0.53, 
                                     0.73, 0.4, 0.37, 0.47, 0.49, 0.5, 0.22, 0.49, 0.66, 0.87]).to(device)
    challenge_thresholds = torch.Tensor([0.05, 0.05, 0.09, 0.05, 0.07, 0.11, 0.16, 0.05, 0.3, 0.1, 0.12, 0.07, 
                            0.05, 0.07, 0.05, 0.45, 0.19, 0.05, 0.22, 0.05, 0.16, 0.06, 0.14, 0.05, 0.06, 0.06]).to(device)
    model.to(device)
    model.eval()
    data = torch.FloatTensor(data[:, :, leads, :])
    features = torch.FloatTensor(features)
    probabilities = run(model, device, data, features)
    probabilities = sigmoid(probabilities)
    labels = torch.gt(probabilities, challenge_thresholds).float()
    return scored_classes[0:26], labels, probabilities

################################################################################
#
# File I/O functions
#
################################################################################

# Save a trained model. This function is not required. You can change or remove it.
def save_model(model_directory, leads, model):
    filename = os.path.join(model_directory, get_model_filename(leads))
    torch.save(model, filename)

# Load a trained model. This function is *required*. You should edit this function to add your code, but do *not* change the arguments of this function.
def load_model(model_directory, leads):
    filename = os.path.join(model_directory, get_model_filename(leads))
    return(torch.load(filename))

# Define the filename(s) for the trained models. This function is not required. You can change or remove it.
def get_model_filename(leads):
    sorted_leads = len(leads)
    return 'model_' + str(sorted_leads) + '.pt'

################################################################################
#
# Feature extraction function
#
################################################################################

# Extract features from the header and recording. This function is not required. You can change or remove it.
def get_features(header, recording, leads):
    # Extract age.
    age = get_age(header)
    if age is None:
        age = float('nan')

    # Extract sex. Encode as 0 for female, 1 for male, and NaN for other.
    sex = get_sex(header)
    if sex in ('Female', 'female', 'F', 'f'):
        sex = 0
    elif sex in ('Male', 'male', 'M', 'm'):
        sex = 1
    else:
        sex = float('nan')

    # Reorder/reselect leads in recordings.
    recording = choose_leads(recording, header, leads)

    # Pre-process recordings.
    adc_gains = get_adc_gains(header, leads)
    baselines = get_baselines(header, leads)
    num_leads = len(leads)
    for i in range(num_leads):
        recording[i, :] = (recording[i, :] - baselines[i]) / adc_gains[i]

    # Compute the root mean square of each ECG lead signal.
    rms = np.zeros(num_leads)
    for i in range(num_leads):
        x = recording[i, :]
        rms[i] = np.sqrt(np.sum(x**2) / np.size(x))

    return age, sex, rms

################################################################################
#
# Filter Function
#
################################################################################
def filter_ecg(recording_raw, header):
    # RECORD anti-aliasing filtering
    recording = ECG_lowpass_anti_aliasing(recording_raw,get_frequency(header),int(get_num_samples(header)))        

    # Amplitude Normalization
    recording_normalized = np.zeros(recording.shape)
    for j in range(recording.shape[0]):
        mean_amp = recording[j,:].mean()
        recording_n1 = (recording[j,:]-mean_amp)
        recording_normalized[j,:] = recording_n1/abs(recording_n1).mean()

    # Resampling (target sampling frequency is 250 Hz)
    recording_resampled = np.zeros((recording.shape[0],int(get_num_samples(header)*250/get_frequency(header))))
    for j in range(recording_resampled.shape[0]):
        if get_frequency(header)%250 == 0:
            resampled_sig = signal.decimate(recording_normalized[j,:],int((get_frequency(header)/250)))
        else:
            upsampled_sig = signal.resample(recording_normalized[j,:],int(get_num_samples(header)*(lcm(get_frequency(header),250)/get_frequency(header))))
            resampled_sig = signal.decimate(upsampled_sig,int(lcm(get_frequency(header),250)/250))

        recording_resampled[j,:] = resampled_sig[0:int(get_num_samples(header)*250/get_frequency(header))]


    # RECORD filtering
    recording_lp = ECG_lowpass(recording_resampled,250,recording_resampled.shape[1])
    recording_filtered = ECG_highpass(recording_lp,250,recording_resampled.shape[1])
    return recording_filtered

################################################################################
#
# Interpolation Function
#
################################################################################
def interp(data):
    nan_idx = np.isnan(data)
    normal_idx = np.logical_not(nan_idx)
    good_data = data[normal_idx]
    interpolated = np.interp(nan_idx.nonzero()[0], normal_idx.nonzero()[0], good_data)
    data[nan_idx] = interpolated
    return data

################################################################################
#
# Dataset and Dataloader
#
################################################################################
class customDataSet(Dataset):
    def __init__(self, x, feat, y):
        self.data = x
        self.labels = y
        self.features = feat
        #self.e_max = ecg_max
        #self.e_min = ecg_min
        #self.max = feature_max
        #self.min = feature_min
    def __len__(self):
        return len(self.labels)
    def __getitem__(self,idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()
        
        sample = self.data[idx]
        feature = self.features[idx]
        labels = self.labels[idx]
        '''
        if self.e_max and self.e_min:
            sample[0] = (sample[0]-self.e_min)/(self.e_max-self.e_min)
        if self.max is not None and self.min is not None:
            for i in range(self.max.shape[0]):
                if (self.max[i]-self.min[i]) == 0:
                    feature[0][i] = 1
                else:
                    feature[0][i] = (feature[0][i]-self.min[i])/(self.max[i]-self.min[i])
        '''
        sample = {'data': torch.FloatTensor(sample), 'feat': torch.FloatTensor(feature), 'labels': torch.FloatTensor(labels)}
        return sample
    
def train_val_split(dataSet, batch_size, num_workers, train_split):
    dSet_size = len(dataSet)
    indices = list(range(dSet_size))
    split = int(np.floor(train_split * dSet_size))
    np.random.shuffle(indices)
    train_indices, val_indices = indices[split:], indices[:split]
    from torch.utils.data.sampler import SubsetRandomSampler
    train_sampler = SubsetRandomSampler(train_indices)
    val_sampler = SubsetRandomSampler(val_indices)

    trainloader = DataLoader(dataSet, batch_size=batch_size, num_workers=num_workers, sampler=train_sampler)
    valloader = DataLoader(dataSet, batch_size=batch_size, num_workers=num_workers, sampler=val_sampler)

    return trainloader, valloader

################################################################################
#
# Training and Validation Functions
#
################################################################################
from ipywidgets import IntProgress
from tqdm.notebook import tqdm
def train(net,device,loader,optimizer,Loss_fun,thresholds):          
    torch.autograd.set_detect_anomaly(True)
    epoch_loss = 0
    correct = 0
    accuracy = 0
    total = 0
    sigmoid = nn.Sigmoid()
    #Set network into training mode 
    net.train() 
    #For each set of data/label in the current loader batch
    with tqdm(total=len(loader)) as pbar:
        for idx, (data, feat, y) in enumerate(loader):
            data, feat, y = data.to(device), feat.to(device), y.to(device)
            #Perform Forward pass of network:
            output = net(data, feat)
            y = y.squeeze(dim=1)
            loss = Loss_fun(output,y)
            optimizer.zero_grad()
            #Back-Prop and step
            loss.backward()
            #torch.nn.utils.clip_grad_norm_(net.parameters(), 0.5)
            #loss.backward(retain_graph = True)
            optimizer.step()
            #increment epoch loss and accuracy
            epoch_loss += loss.item()
            total += y.size(0)
            output = sigmoid(output).detach().cpu().numpy()
            output = np.greater_equal(output, thresholds).astype(float)
            accuracy += accuracy_score(y.cpu().numpy(), output, normalize=False)
            pbar.update(1)
        return epoch_loss/len(loader), (accuracy/total)*100

def validate(net,device, loader, Loss_fun, thresholds):
    epoch_loss = 0
    correct = 0
    accuracy = 0
    total = 0
    sigmoid = nn.Sigmoid()
    #Set network into evaluation mode
    net.eval()
    
    with torch.no_grad():
        for idx, (data,feat,y) in enumerate(loader):
            data, feat, y = data.to(device), feat.to(device), y.to(device)
            y = y.squeeze(dim=1)
            output = net(data, feat)
            loss = Loss_fun(output,y)
            epoch_loss += loss.item()
            total += y.size(0) # sums the number of predictions
            output = sigmoid(output).detach().cpu().numpy()
            output = np.greater_equal(output, thresholds).astype(float)
            accuracy += accuracy_score(y.cpu().numpy(), output, normalize=False)
        return epoch_loss/len(loader), (accuracy/total)*100

def run(net, device, data, feat):
    #Set network into evaluation mode
    data, feat = data.to(device), feat.to(device)
    output = net(data,feat)
    return output

# Reset weights for K-Fold Validation
def reset_weights(m):
    '''
    Try resetting model weights to avoid
    weight leakage.
    '''
    for layer in m.children():
        if hasattr(layer, 'reset_parameters'):
            layer.reset_parameters()

################################################################################
#
# Disable and Enable Print (((((CHANGE FOR PY SCRIPT)))))
#
################################################################################
def blockPrint():
    sys.stdout = open(os.devnull, 'w')

def enablePrint():
    sys.stdout = sys.__stdout__